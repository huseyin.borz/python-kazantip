# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


# unit tests for generated queries by Specs

from typing import Type

ARG = 1
MOD_SPEC_TEST_DATA = {
    "a": 1,
    "b": "2",
    "c": [1, 2, 3],
}
MOD_SPEC_TEST_FILTER_IDS = [1, 2, 3]


def test_table_query(test_select_spec: Type['TestSelectSpec']):
    spec = test_select_spec(my_arg=ARG)
    sql, args = spec.collect_q_str

    assert sql == 'SELECT "table"."fld1" AS "fld1", ROW_TO_JSON(ROW("table"."fld2", "table"."fld3")) AS "fld2" FROM "table" WHERE "table"."fld1" = $1'
    assert args == [ARG, ]


def test_fields_query(test_select_spec: Type['TestSelectSpec']):
    spec = test_select_spec(fields=['fld1', ], my_arg=ARG)
    sql, args = spec.collect_q_str

    assert '"fld2"' not in sql
    assert '"ROW_TO_JSON"' not in sql
    assert args == [ARG, ]


def test_ids_query(test_select_spec: Type['TestSelectSpec']):
    spec = test_select_spec(ids=[1, 2, 3], my_arg=ARG)
    sql, args = spec.collect_q_str

    assert 'JOIN UNNEST(ARRAY[1,2,3]) AS "id" ON ("table"."id" = CAST("id" AS bigint))' in sql
    assert args == [ARG, ]


def test_insert_query(test_mod_spec: Type['TestModSpec']):
    spec = test_mod_spec(MOD_SPEC_TEST_DATA)
    sql, args = spec.save_q_str

    assert sql == 'INSERT INTO "table" ("fld", "dup_fld", "fld_conflict") VALUES ($1, $2, ARRAY[1,2,3]) ON CONFLICT ("dup_fld") DO UPDATE SET "fld_conflict" = "excluded"."fld_conflict" RETURNING "fld", "dup_fld", "fld_conflict"'
    assert args == [1, '2']


def test_update_query(test_mod_spec: Type['TestModSpec']):
    spec = test_mod_spec(MOD_SPEC_TEST_DATA, MOD_SPEC_TEST_FILTER_IDS)
    sql, args = spec.update_q_str

    assert sql == 'UPDATE "table" SET "fld" = $1, "dup_fld" = $2, "fld_conflict" = ARRAY[1,2,3] WHERE ("table"."fld" IN ($3, $4, $5)) RETURNING "fld", "dup_fld", "fld_conflict"'
    assert args == [1, '2', 1, 2, 3]


def test_delete_query(test_mod_spec: Type['TestModSpec']):
    spec = test_mod_spec(filter_ids=MOD_SPEC_TEST_FILTER_IDS)
    sql, args = spec.delete_q_str

    assert sql == 'DELETE FROM "table" WHERE ("table"."fld" IN ($1, $2, $3)) RETURNING "fld", "dup_fld", "fld_conflict"'
    assert args == MOD_SPEC_TEST_FILTER_IDS


def test_insert_subq_query(test_mod_subq_spec: Type['TypeModSubqSpec']):
    spec = test_mod_subq_spec(MOD_SPEC_TEST_DATA)
    sql, args = spec.save_q_str

    assert sql == 'INSERT INTO "table" ("fld", "dup_fld", "fld_conflict") VALUES ($1, $2, (SELECT ARRAY_AGG($3) FROM "other_table"))'
    assert args == [1, '2', 1]


def test_update_subq_query(test_mod_subq_spec: Type['TypeModSubqSpec']):
    spec = test_mod_subq_spec(MOD_SPEC_TEST_DATA)
    sql, args = spec.update_q_str

    assert sql == 'UPDATE "table" SET "fld" = $1, "dup_fld" = $2, "fld_conflict" = (SELECT ARRAY_AGG($3) FROM "other_table")'
    assert args == [1, '2', 1]
