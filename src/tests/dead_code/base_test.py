# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import pytest
from schema import Schema

import ujson

from .schemas.errors import error_schema


@pytest.mark.usefixtures('app_client', 'app_server')
class TestBase(object):
    __test__ = False
    route_alias: str = ''
    required_fields: dict = {}
    other_fields: dict = {}
    exclude_field: list = []
    schema: Schema = None
    with_pagination: bool = False
    with_aggregation: bool = False

    async def test_fail_required_fields(self, app, app_client):
        if len(self.required_fields) < 2:
            return
        route = app.router.get(self.route_alias)
        assert route is not None
        for field, value in self.required_fields.items():
            payload = {
                f'{field}': f'{value}'
            }
            url = route.url_for().with_query(payload)
            resp = await app_client.get(url)
            assert resp.status == 400

    async def test_fail_required_fields_bad_value(self, app, app_client):
        route = app.router.get(self.route_alias)
        assert route is not None
        for field, value in self.required_fields.items():
            payload = self.required_fields.copy()
            if field in self.exclude_field:
                continue
            payload[field] = "FAIL"
            url = route.url_for().with_query(payload)
            resp = await app_client.get(url)
            assert resp.status == 400

    async def test_required_fields(self, app, app_client):
        route = app.router.get(self.route_alias)
        assert route is not None
        url = route.url_for().with_query(self.required_fields)
        resp = await app_client.get(url)
        assert resp.status == 200
        data = ujson.loads(await resp.read())
        validate = self.schema.validate(data)
        assert validate

    async def test_other_fields(self, app, app_client):
        route = app.router.get(self.route_alias)
        assert route is not None
        for field, value in self.other_fields.items():
            payload = self.required_fields.copy()
            payload[field] = value
            url = route.url_for().with_query(payload)
            resp = await app_client.get(url)
            assert resp.status == 200
            data = ujson.loads(await resp.read())
            validate = self.schema.validate(data)
            assert validate

    async def test_ok(self, app, app_client):
        route = app.router.get(self.route_alias)
        assert route is not None
        payload = {**self.required_fields, **self.other_fields}
        url = route.url_for().with_query(payload)
        resp = await app_client.get(url)
        assert resp.status == 200
        data = ujson.loads(await resp.read())
        validate = self.schema.validate(data)
        assert validate

    async def test_fields(self, app, app_client):
        if 'fields' not in self.other_fields:
            pytest.skip("Test witout fields required")
        route = app.router.get(self.route_alias)
        assert route is not None
        payload = self.required_fields.copy()
        payload['fields'] = self.other_fields['fields'] + ",bad_field"
        url = route.url_for().with_query(payload)
        resp = await app_client.get(url)
        assert resp.status == 400  # поле title невалидное

    async def test_pagination_overflow(self, app, app_client):
        if not self.with_pagination:
            pytest.skip("Test witout paginations")
        route = app.router.get(self.route_alias)
        assert route is not None

        payload = {
            'itemsPerPage': 10,
            'startIndex': 10000000
        }
        payload.update(self.required_fields)
        url = route.url_for().with_query(payload)
        resp = await app_client.get(url)
        data = ujson.loads(await resp.read())
        error_schema.validate(data)
        assert resp.status == 400

    async def test_pagination_empty_response(self, app, app_client):
        if not self.with_pagination:
            pytest.skip("Test witout paginations")
        route = app.router.get(self.route_alias)
        assert route is not None

        payload = {
            'itemsPerPage': 0,
            'startIndex': 0
        }
        payload.update(self.required_fields)
        url = route.url_for().with_query(payload)
        resp = await app_client.get(url)
        data = ujson.loads(await resp.read())
        validate = self.schema.validate(data)
        assert validate
        assert resp.status == 200

    async def test_pagination_negative_values(self, app, app_client):
        if not self.with_pagination:
            pytest.skip("Test witout paginations")
        route = app.router.get(self.route_alias)
        assert route is not None

        payload = {
            'itemsPerPage': -1,
            'startIndex': -1
        }
        payload.update(self.required_fields)
        url = route.url_for().with_query(payload)
        resp = await app_client.get(url)
        data = ujson.loads(await resp.read())
        validate = error_schema.validate(data)
        assert validate
        assert resp.status == 400

    async def test_pagination_item_count_values(self, app, app_client):
        if not self.with_pagination:
            pytest.skip("Test witout paginations")
        route = app.router.get(self.route_alias)
        assert route is not None

        # currentItemCount == len(items)
        payload = {
            'itemsPerPage': 5,
            'startIndex': 0,
        }
        payload.update(self.required_fields)
        url = route.url_for().with_query(payload)
        resp = await app_client.get(url)
        data = ujson.loads(await resp.read())
        validate = self.schema.validate(data)
        assert validate
        assert data['data']['currentItemCount'] == len(data['data']['items'])
        assert resp.status == 200

        # get all items and compare totalItems with currentItemCount
        payload['itemsPerPage'] = data['data']['totalItems']
        payload['startIndex'] = 0
        url = route.url_for().with_query(payload)
        resp = await app_client.get(url)
        data = ujson.loads(await resp.read())
        validate = self.schema.validate(data)
        assert validate
        assert data['data']['currentItemCount'] == len(data['data']['items']) == data['data']['totalItems']
        assert resp.status == 200
