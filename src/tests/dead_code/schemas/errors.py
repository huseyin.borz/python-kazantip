# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


from schema import Optional, Schema

error_item_model = {
    Optional("domain"): str,
    "reason": str,
    Optional("message"): str,
    Optional("location"): str,
    Optional("locationType"): str,
    Optional("extendedHelp"): str,
    Optional("sendReport"): str
}

error_model = {
    "code": int,
    "message": str,
    Optional("errors"): [error_item_model]
}

error_schema = Schema({
    Optional("apiVersion"): str,
    Optional("id"): str,
    "error": error_model
})
