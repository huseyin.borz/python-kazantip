# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


from aiohttp.web import Response

from conf.settings import SERVICE_NAME


async def healthcheck(_):
    return Response(text='OK')


async def index(_):
    return Response(text=SERVICE_NAME)
