# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


try:
    import uvloop
except ImportError:
    uvloop = None

import asyncio
import logging
import os

from aiohttp.web import Application, run_app

# [NGibaev 04.03.19] this import is necessary bcs only with ``dramatiq_conf`` evaluated
# WEB_SERVER <-> BROKER configuration
import conf.dramatiq_conf  # noqa
from common.middlewares import get_middlewares
from conf import lazy_init, settings
from conf.db import setup_database
from conf.routes import setup_routes


async def build_application(app=None) -> Application:
    app = app or Application(middlewares=get_middlewares())
    await setup_database(app)

    await lazy_init.init_lazy_settings(app)
    setup_routes(app)

    return app


if __name__ == '__main__':
    logger = logging.getLogger('main')

    #  Если не windows ставим EventLoopPolicy
    if os.name != 'nt':
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    loop = asyncio.get_event_loop()
    web_app = loop.run_until_complete(build_application())
    logger.info(f'serving on http://{settings.LISTEN_HOST}:{settings.LISTEN_PORT}')
    run_app(web_app, host=settings.LISTEN_HOST, port=settings.LISTEN_PORT)
