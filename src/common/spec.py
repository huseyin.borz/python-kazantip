# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import logging
from collections import OrderedDict
from typing import List, Tuple, Union

from sqlbuilder.smartsql.expressions import (Array, Constant, Param,
                                             compile_exprlist)

from .custom_spec_compiler import *
from .sqlbuilder_common import Cast, F, Q, T, TableJoin, V, compile, func, Binary

__all__ = ('Spec', 'ModifyingSpec', 'ModifyingSpecSQL')
logger = logging.getLogger(__name__)


class UnknownPrepareDataFormatException(Exception):
    pass


# [NGibaev 28.08.18] i have to use such KOSTYL for working around django-specified default compiler
def get_compiled(q: Q, dml: bool=True) -> Tuple[str, list]:
    res = None
    if dml:
        sql, args = compile(q)
        if args:
            # [NGibaev 28.08.18] first item is compiled
            res = args[0], args[1:]
        else:
            res = sql, args
    else:
        res = compile(q)
    logger.debug(f'GENERATED SQL: {res[0]} with params {res[1]}')
    return res


class Spec:
    """
    Abstract class for specifications which are used only for data retrieval
    Declare fields, body of query and possible filters for data and total retrieval.
    Main usage of concrete spec mainly by calling ``collect_q`` and ``total_q`` instance methods

    ids_filter: tuple with filter table or tablejoin expression, left and right join expressions
    for filtering by identifiers

    main_tables: table or tablejoin expression for retrieval data

    fields: OrderedDict of tuples, where each tuple contains key of field (which will be used
    in fields=... key in API), field declaration as value
    """
    # [NGibaev 11.04.19] since python 3.6 all dicts preserve order in iterators,
    # OrderedDict is obsolete
    _fields: Union[OrderedDict, dict]

    def __init__(self, *args, ids: List[object]=None, limit: int=0, offset: int=0, fields: List[str]=None, **kwargs):
        self.ids = list(ids) if ids else None
        self.limit = limit
        self.offset = offset
        # [NGibaev 28.08.18] still unused?
        self.params = []
        self.req_fields = fields if fields else []
        self.__is_prepared = False
        self.defer_fields_init()

    def __prepare(self):
        if self.ids:
            ids_filter_tbl = T(func.unnest(Array(self.ids)))
            ids_filter_tbl, ids_filter_attr_l, right_type = self.ids_filter(ids_filter_tbl)
            # [NGibaev 11.04.19] sometimes compiler messes with order of left and right operands order so i had to use explicit Binary
            self.__tables = \
                T(self.main_tables() & ids_filter_tbl). \
                on(Binary(ids_filter_attr_l, '=', Cast(ids_filter_tbl, right_type)))
            # self.params += [self.ids]
        else:
            self.__tables = T(self.main_tables())
        self.__is_prepared = True

    def __required_fields(self) -> List[F]:
        if self.req_fields:
            return [f.as_(key) for (key, f) in self.fields.items()
                    if key in self.req_fields]
        else:
            return [f.as_(key) for (key, f) in self.fields.items()]

    def __check_prepared(self):
        if not self.__is_prepared:
            self.__prepare()

    def defer_fields_init(self):
        pass

    def ids_filter(self, ids: list) -> Tuple[Union[T, TableJoin], F, F]:
        raise NotImplementedError()

    def main_tables(self) -> Union[T, TableJoin]:
        raise NotImplementedError()

    @property
    def collect_q_str(self) -> Tuple[str, list]:
        """
        Get SQL query for retrieval data and list of arguments for query
        """
        return get_compiled(self.collect_q, False)

    @property
    def collect_q(self) -> Tuple[Q, list]:
        self.__check_prepared()
        q = Q().fields(*self.__required_fields()).tables(self.__tables).group_by(self.to_group())
        q = self.apply_criteria(q)
        if self.limit is not None:
            q = q[self.offset:self.offset + self.limit]
        return q

    @property
    def total_q_str(self) -> Tuple[str, list]:
        """
        Get SQL query for retrieval count of rows in result set and arguments for query
        """
        self.__check_prepared()
        q = Q().fields(F(func.COUNT(V(1)))).tables(T(self.collect_q.limit(None)).as_('cnt'))
        return get_compiled(q, False)

    @property
    def fields(self) -> OrderedDict:
        return self._fields

    def to_group(self) -> List[F]:
        return []

    def apply_criteria(self, q: Q) -> Q:
        return q


class ModifyingSpec:
    """
    Abstract class for specifications which are used for data modification
    Declare table where specified data would be inserted in ``_target_tbl`` attribute. Implement ``prepare_data``
    method in order to convert in database driver-friendly format (base types like str, int, lists, dicts etc. are allowed)

    In order to chain multiple data modifying spec you can use 'around' attributes which are passed in contructor:
    ``before_specs`` and ``after_specs`` arguments. Keep in mind that if you plan to execute multiple data modifications
    independently its HIGHLY recommended to use database transactions to keep track of stored data and recover from errors gracefully.

    ``use_method`` arg in constructor used only if main ModifyingSpec tries to glue multiple ModifyingSpec's.

    kwargs are passed to spec compilation.
    For ``INSERT`` it would be ``ignore`` and ``on_duplicate_key_update`` (ON CONFLICT resolutions)
    For ``UPDATE`` it would be nothing.
    For ``DELERE`` it would be nothing.

    """

    _target_tbl: T = None
    _insert_on_conflict: dict = None
    _duplicate_key: tuple = None
    _delete_cascade: bool = False
    _fields: Tuple[F] = ()
    _returning: Tuple[F] = ()

    def __init__(self, data=None, **kwargs):
        """
        :param data: target payload for ``INSERT`` and ``UPDATE``

        :param kwargs: additional keyword parameters that passed to query instance of sqlbuilder
        """
        self.source_data = data
        self.kwargs = kwargs

    def main_tables(self) -> Union[T, TableJoin]:
        raise NotImplementedError()

    def prepare_data(self, data):
        raise NotImplementedError()

    def prepare_data_update(self, data):
        raise NotImplementedError()

    @property
    def save_q(self) -> Q:
        q = Q().tables(self._target_tbl)
        if self._fields:
            q = q.fields(*self._fields)
        data = self.prepare_data(self.source_data)
        if self._insert_on_conflict:
            self.kwargs.update(dict(
                on_duplicate_key_update=self._insert_on_conflict,
                duplicate_key=self._duplicate_key
            ))
        if self._returning:
            self.kwargs.update(dict(returning_fields=self._returning))
        if isinstance(data, dict):
            return q.insert(data, **self.kwargs)
        elif isinstance(data, (list, Q)):
            return q.insert(values=data, **self.kwargs)
        elif isinstance(data, tuple):
            # [NGibaev 11.04.19] reformat tuple bcs one-dimensional tuple compiles
            # into one set with rows of 1 values in each row which is not correct
            return q.insert(values=(data, ), **self.kwargs)
        else:
            raise UnknownPrepareDataFormatException()

    @property
    def save_q_str(self) -> str:
        return get_compiled(self.save_q)

    @property
    def update_q(self) -> Q:
        q = self.apply_criteria(Q().tables(self._target_tbl))
        if self._fields:
            q = q.fields(*self._fields)
        data = self.prepare_data_update(self.source_data)
        if self._returning:
            self.kwargs.update(dict(returning_fields=self._returning))
        if isinstance(data, dict):
            return q.update(data, **self.kwargs)
        # [NGibaev 11.04.19] include Q here?
        elif isinstance(data, list):
            return q.update(values=self.data, **self.kwargs)
        else:
            raise UnknownPrepareDataFormatException()

    @property
    def update_q_str(self) -> str:
        return get_compiled(self.update_q)

    @property
    def delete_q(self) -> Q:
        q = self.apply_criteria(Q().tables(self._target_tbl))
        if self._returning:
            self.kwargs.update(dict(returning_fields=self._returning))
        return q.delete(**self.kwargs)

    @property
    def delete_q_str(self) -> str:
        return get_compiled(self.delete_q)

    def apply_criteria(self, q: Q) -> Q:
        return q


class ModifyingSpecSQL:
    collect_q: Q
    save_q: Q
    save_resp_q: Q
    update_q: Q
    update_resp_q: Q
    delete_q: Q
    delete_resp_q: Q
    _target_tbl: T
    _insert_on_conflict: dict
    _delete_cascade: bool
    '''Fields is a alias how it exist in db'''
    _fields: dict

    # [NGibaev 28.08.18] probably extract use_method to SpecsChain or superclass
    def __init__(self, data, use_method: str = None, **kwargs):
        self.method = use_method
        self.source_data = data
        self.kwargs = kwargs

    def _get_fields_query(self, fields: Tuple[str]) -> str:
        if fields:
            query = ', '.join('%s as %s' % (v, k)
                              for k, v in fields
                              if self._fields.values())
        else:
            query = ', '.join('%s as %s' % (v, k)
                              for k, v in self._fields.items())
        return query

    def defer_fields_init(self):
        pass

    def prepare_data(self, data):
        raise NotImplementedError()

    def prepare_data_update(self, data):
        raise NotImplementedError()

    @property
    def save_q_str(self) -> Tuple[str, list]:
        raise NotImplementedError()

    @property
    def update_q_str(self) -> Tuple[str, list]:
        raise NotImplementedError()

    @property
    def delete_q_str(self) -> Tuple[str, list]:
        raise NotImplementedError()

    @property
    def collect_q_str(self) -> Tuple[str, list]:
        raise NotImplementedError()

    @property
    def total_q_str(self) -> Tuple[str, list]:
        raise NotImplementedError()

    @property
    def save_object_q_str(self) -> Tuple[str, list]:
        raise NotImplementedError()

    @property
    def update_object_q_str(self) -> Tuple[str, list]:
        raise NotImplementedError()

    @property
    def delete_object_q_str(self) -> Tuple[str, list]:
        raise NotImplementedError()

    def apply_criteria(self, q: Q) -> Q:
        return q
