# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


from sqlbuilder.smartsql.compiler import compile
from sqlbuilder.smartsql.constants import CONTEXT
from sqlbuilder.smartsql.exceptions import Error
from sqlbuilder.smartsql.expressions import (Array, Constant, Expr, ExprList,
                                             OmitParentheses, Operable,
                                             Parentheses, Value, expr_repr,
                                             func)
from sqlbuilder.smartsql.factory import factory
from sqlbuilder.smartsql.fields import Field, FieldList
from sqlbuilder.smartsql.operators import Asc, Desc
from sqlbuilder.smartsql.pycompat import string_types
from sqlbuilder.smartsql.queries import SPACE, Modify, Query
from sqlbuilder.smartsql.tables import TableJoin
from sqlbuilder.smartsql.utils import is_list, opt_checker, same, warn


class SQLParam:
    def __init__(self, contents):
        self.contents = contents

# [NGibaev 28.08.18] patched compiler for using asyncpg params
@compile.when(object)
def compile_object(compile, expr, state):
    if not state.__dict__.get('i_arg'):
        state.__dict__['i_arg'] = 0
    state.__dict__['i_arg'] += 1
    state.sql.append('$' + str(state.__dict__['i_arg']))
    state.params.append(expr)


@compile.when(SQLParam)
def compile_sqlparam(compile, expr, state):
    if not state.__dict__.get('i_arg'):
        state.__dict__['i_arg'] = 0
    state.__dict__['i_arg'] += 1
    state.sql.append('$' + str(state.__dict__['i_arg']))
    state.params.append(expr.contents)


trans = {
    int: Constant,
    str: Value,
}


def transtype(expr):
    for e in expr.data[0]:
        yield trans[type(e)](str(e))


# [NGibaev 28.08.18] need to resolve arrays troubles in sqlbuilder package
@compile.when(Array)
def compile_array(compile, expr, state):
    if not expr.data:
        state.sql.append("'{}'")
    inside = ','.join(compile(e)[0] for e in transtype(expr))
    state.sql.append(f"ARRAY[{inside}]")


@factory.register
class Insert(Modify):
    def __init__(self, table, mapping=None, fields=None, values=None, ignore=False, on_duplicate_key_update=None, duplicate_key=None, returning_fields=None):
        self.table = table
        self.fields = FieldList(*(k if isinstance(k, Expr) else table.get_field(k) for k in (mapping or fields)))
        self.values = (tuple(mapping.values()),) if mapping else values
        self.ignore = ignore
        self.on_duplicate_key_update = tuple(
            (k if isinstance(k, Expr) else table.get_field(k), v)
            for k, v in on_duplicate_key_update.items()
        ) if on_duplicate_key_update else None
        if duplicate_key:
            if not is_list(duplicate_key):
                duplicate_key = (duplicate_key,)
            self.duplicate_key = FieldList(*(i if isinstance(i, Expr) else table.get_field(i) for i in duplicate_key))
        else:
            self.duplicate_key = None
        self.returning_fields = returning_fields


@compile.when(Insert)
def compile_insert(compile, expr, state):
    state.push("context", CONTEXT.TABLE)
    state.sql.append("INSERT ")
    state.sql.append("INTO ")
    compile(expr.table, state)
    state.sql.append(SPACE)

    state.context = CONTEXT.FIELD_NAME
    compile(Parentheses(expr.fields), state)
    state.context = CONTEXT.EXPR
    if isinstance(expr.values, Query):
        state.sql.append(SPACE)
        compile(expr.values, state)
    else:
        state.sql.append(" VALUES ")
        compile(ExprList(*expr.values).join(', '), state)
    if expr.ignore:
        state.sql.append(" ON CONFLICT DO NOTHING")
    elif expr.on_duplicate_key_update:
        state.sql.append(" ON CONFLICT")
        if expr.duplicate_key:
            state.sql.append(SPACE)
            state.context = CONTEXT.FIELD_NAME
            compile(Parentheses(expr.duplicate_key), state)
            state.context = CONTEXT.EXPR
        state.sql.append(" DO UPDATE SET ")
        first = True
        for f, v in expr.on_duplicate_key_update:
            if first:
                first = False
            else:
                state.sql.append(", ")
            state.context = CONTEXT.FIELD_NAME
            compile(f, state)
            state.context = CONTEXT.EXPR
            state.sql.append(" = ")
            compile(v, state)
    if expr.returning_fields:
        state.context = CONTEXT.FIELD_NAME
        state.sql.append(" RETURNING ")
        first = True
        for f in expr.returning_fields:
            if first:
                first = False
            else:
                state.sql.append(", ")
            compile(f, state)
    state.pop()


@factory.register
class Update(Modify):
    def __init__(self, table, mapping=None, fields=None, values=None, ignore=False, where=None, order_by=None, limit=None, returning_fields=None):
        self.table = table
        self.fields = FieldList(*(k if isinstance(k, Expr) else table.get_field(k) for k in (mapping or fields)))
        self.values = tuple(mapping.values()) if mapping else values
        self.ignore = ignore
        self.where = where
        self.order_by = order_by
        self.limit = limit
        self.returning_fields = returning_fields


@compile.when(Update)
def compile_update(compile, expr, state):
    state.push("context", CONTEXT.TABLE)
    state.sql.append("UPDATE ")
    if expr.ignore:
        state.sql.append("IGNORE ")
    compile(expr.table, state)
    state.sql.append(" SET ")
    first = True
    for field, value in zip(expr.fields, expr.values):
        if first:
            first = False
        else:
            state.sql.append(", ")
        state.context = CONTEXT.FIELD_NAME
        compile(field, state)
        state.context = CONTEXT.EXPR
        state.sql.append(" = ")
        compile(value, state)
    state.context = CONTEXT.EXPR
    if expr.where:
        state.sql.append(" WHERE ")
        compile(expr.where, state)
    if expr.order_by:
        state.sql.append(" ORDER BY ")
        compile(expr.order_by, state)
    if expr.limit is not None:
        state.sql.append(" LIMIT ")
        compile(expr.limit, state)
    if expr.returning_fields:
        state.sql.append(" RETURNING ")
        first = True
        for f in expr.returning_fields:
            if first:
                first = False
            else:
                state.sql.append(", ")
            state.context = CONTEXT.FIELD_NAME
            compile(f, state)
            state.context = CONTEXT.EXPR
    state.pop()


@factory.register
class Delete(Modify):
    def __init__(self, table, where=None, order_by=None, limit=None, returning_fields=None):
        self.table = table
        self.where = where
        self.order_by = order_by
        self.limit = limit
        self.returning_fields = returning_fields


@compile.when(Delete)
def compile_delete(compile, expr, state):
    state.sql.append("DELETE FROM ")
    state.push("context", CONTEXT.TABLE)
    compile(expr.table, state)
    state.context = CONTEXT.EXPR
    if expr.where:
        state.sql.append(" WHERE ")
        compile(expr.where, state)
    if expr.order_by:
        state.sql.append(" ORDER BY ")
        compile(expr.order_by, state)
    if expr.limit is not None:
        state.sql.append(" LIMIT ")
        compile(expr.limit, state)
    if expr.returning_fields:
        state.sql.append(" RETURNING ")
        first = True
        for f in expr.returning_fields:
            if first:
                first = False
            else:
                state.sql.append(", ")
            state.context = CONTEXT.FIELD_NAME
            compile(f, state)
            state.context = CONTEXT.EXPR
    state.pop()
