# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


class BaseHttpException(Exception):
    status_code: int = 500
    message: str = "Server Error"
    reason: str = ''

    def __init__(self, *args):
        super().__init__(self, self.message, *args)
        self.text = f"Code error {self.status_code}: {self.message}: {self.reason}"

    def __repr__(self):
        return self.text

    def __str__(self):
        return self.text


class HttpException(BaseHttpException):
    def __init__(self, domain: str = None, message: str = None, reason: str = None, location: str = None,
                 location_type: str = None, status_code: int = None, extended_help: str = None,
                 send_report: str = None,  *args):
        if location:
            self.location = location
        if reason:
            self.reason = reason
        if domain:
            self.domain = domain
        if location_type:
            self.location_type = location_type
        if extended_help:
            self.extended_help = extended_help
        if send_report:
            self.send_report = send_report
        if status_code:
            self.status_code = status_code
        if message:
            self.message = message

        super().__init__(self.message, *args)


class AggregatorHttpExceptions(BaseHttpException):
    def __init__(self, status_code: int = 500, message: str = None, errors: list = None,  *args):
        self.exceptions = errors
        self.status_code = status_code
        if message:
            self.message = message
        super().__init__(*args)


class HTTPError(HttpException):
    """Base class for exceptions with status codes in the 400s and 500s."""


class HTTPRedirection(HttpException):
    """Base class for exceptions with status codes in the 300s."""


class HTTPSuccessful(HttpException):
    """Base class for exceptions with status codes in the 200s."""


class HTTPOk(HTTPSuccessful):
    status_code = 200
    message = "Successful"


class HTTPCreated(HTTPSuccessful):
    status_code = 201


class HTTPAccepted(HTTPSuccessful):
    status_code = 202


class HTTPNonAuthoritativeInformation(HTTPSuccessful):
    status_code = 203


class HTTPNoContent(HTTPSuccessful):
    status_code = 204


class HTTPResetContent(HTTPSuccessful):
    status_code = 205


class HTTPPartialContent(HTTPSuccessful):
    status_code = 206


############################################################
# 3xx redirection
############################################################


class HTTPMultipleChoices(HTTPRedirection):
    status_code = 300


class HTTPMovedPermanently(HTTPRedirection):
    status_code = 301


class HTTPFound(HTTPRedirection):
    status_code = 302


class HTTPSeeOther(HTTPRedirection):
    status_code = 303


class HTTPNotModified(HTTPRedirection):
    status_code = 304


class HTTPUseProxy(HTTPRedirection):
    status_code = 305


class HTTPTemporaryRedirect(HTTPRedirection):
    status_code = 307


class HTTPPermanentRedirect(HTTPRedirection):
    status_code = 308


############################################################
# 4xx client error
############################################################


class HTTPClientError(HTTPError):
    pass


class HTTPBadRequest(HTTPClientError):
    status_code = 400


class PaginationOverflow(HTTPClientError):
    status_code = 400
    
    def __init__(self, offset=None, total=None, *args, **kwargs):
        message = kwargs.pop('message', None)
        if not message:
            message = f'Page overflow! startIndex:{offset} >= totalItems:{total}'
        super().__init__(message=message, *args, **kwargs)


class HTTPUnauthorized(HTTPClientError):
    status_code = 401
    message = "Unauthorized"


class HTTPPaymentRequired(HTTPClientError):
    status_code = 402


class HTTPForbidden(HTTPClientError):
    status_code = 403
    message = "Forbidden"


class HTTPNotFound(HTTPClientError):
    status_code = 404
    message = "Not Found"


class HTTPMethodNotAllowed(HTTPClientError):
    status_code = 405


class HTTPNotAcceptable(HTTPClientError):
    status_code = 406


class HTTPProxyAuthenticationRequired(HTTPClientError):
    status_code = 407


class HTTPRequestTimeout(HTTPClientError):
    status_code = 408


class HTTPConflict(HTTPClientError):
    status_code = 409


class HTTPGone(HTTPClientError):
    status_code = 410


class HTTPLengthRequired(HTTPClientError):
    status_code = 411


class HTTPPreconditionFailed(HTTPClientError):
    status_code = 412


class HTTPRequestEntityTooLarge(HTTPClientError):
    status_code = 413


class HTTPRequestURITooLong(HTTPClientError):
    status_code = 414


class HTTPUnsupportedMediaType(HTTPClientError):
    status_code = 415


class HTTPRequestRangeNotSatisfiable(HTTPClientError):
    status_code = 416


class HTTPExpectationFailed(HTTPClientError):
    status_code = 417


class HTTPMisdirectedRequest(HTTPClientError):
    status_code = 421


class HTTPUnprocessableEntity(HTTPClientError):
    status_code = 422


class HTTPFailedDependency(HTTPClientError):
    status_code = 424


class HTTPUpgradeRequired(HTTPClientError):
    status_code = 426


class HTTPPreconditionRequired(HTTPClientError):
    status_code = 428


class HTTPTooManyRequests(HTTPClientError):
    status_code = 429


class HTTPRequestHeaderFieldsTooLarge(HTTPClientError):
    status_code = 431


class HTTPUnavailableForLegalReasons(HTTPClientError):
    status_code = 451


############################################################
# 5xx Server Error
############################################################
#  Response status codes beginning with the digit "5" indicate cases in
#  which the server is aware that it has erred or is incapable of
#  performing the request. Except when responding to a HEAD request, the
#  server SHOULD include an entity containing an explanation of the error
#  situation, and whether it is a temporary or permanent condition. User
#  agents SHOULD display any included entity to the user. These response
#  codes are applicable to any request method.


class HTTPServerError(HTTPError):
    pass


class HTTPInternalServerError(HTTPServerError):
    status_code = 500


class HTTPNotImplemented(HTTPServerError):
    status_code = 501


class HTTPBadGateway(HTTPServerError):
    status_code = 502


class HTTPServiceUnavailable(HTTPServerError):
    status_code = 503


class HTTPGatewayTimeout(HTTPServerError):
    status_code = 504


class HTTPVersionNotSupported(HTTPServerError):
    status_code = 505


class HTTPVariantAlsoNegotiates(HTTPServerError):
    status_code = 506


class HTTPInsufficientStorage(HTTPServerError):
    status_code = 507


class HTTPNotExtended(HTTPServerError):
    status_code = 510


class HTTPNetworkAuthenticationRequired(HTTPServerError):
    status_code = 511
