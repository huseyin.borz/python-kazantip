# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


from typing import Callable, Dict, TypeVar, Union, List

import inflection

AnyObj = TypeVar(str, int, dict, list, tuple)


class DictWithoutNoneValue(dict):
    def __setitem__(self, key, value):
        if key in self or value is not None:
            dict.__setitem__(self, key, value)


def camel_to_underscore(name: str):
    """
    Change naming convention for string

    :param name: string to convert
    :return: string in ``snake_case`` format
    """
    return inflection.underscore(name)


def underscore_to_camel(name: str):
    """
    Change naming convention for string

    :param name: string to convert
    :return: string in ``camelCase`` format
    """
    return inflection.camelize(name, False)


def change_dict_naming_convention(d: AnyObj, convert_function: Callable = underscore_to_camel) -> AnyObj:
    """
    Changes dict keys format between ``camelCase`` <-> ``snake_case``. Applies changes ``recursive`` so all dict keys in all nested objects will be affected, KIM.

    :param d: any object. only container types are affected
    :param convert_function: callable with arity /1 which converts string to some format.
    :return: instance of d with change
    """
    if isinstance(d, (list, tuple)):
        return [change_dict_naming_convention(x, convert_function) for x in d]
    elif isinstance(d, dict):
        return {convert_function(k): change_dict_naming_convention(v, convert_function)
                for k, v in d.items()}
    else:
        return d


def dict_dig(d: Union[dict, list], keys_path: List[object], missing=None):
    """
    ``Digs`` into given nested structure and returns either value by given path or ``missing`` value.

    :param d: target container instance
    :param keys_path: list of keys in which order to navigate inside dict
    :param missing: value to return if value by given path is not present
    :return: instance of dict
    """
    curr = d
    for k in keys_path:
        if isinstance(curr, dict) and k in curr:
            curr = curr[k]
        elif isinstance(curr, (list, tuple)) and k < len(curr):
            curr = curr[k]
        else:
            return missing
    return curr
