# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import functools
from collections import OrderedDict

from aiohttp.web_request import Request
from aiohttp.web_response import json_response

import ujson
from common.utils.exceptions import AggregatorHttpExceptions
from conf import settings

from .exception_dict import exception_dict
from .utils import DictWithoutNoneValue, change_dict_naming_convention

__all__ = ["return_json", "api_call", "serialize_exception"]


def return_json(data: dict, status: int = 200):
    return json_response(data,
                         status=status,
                         dumps=ujson.dumps)


class ApiCall(object):
    def __init__(self, func, this: object, method: str = None):
        self.func = func
        self.context = None
        self.id = None
        self.method = method if method else this.__class__.__name__ + "->" + func.__name__
        self.data = None
        self.api_version = settings.API_VERSION
        self.this = this
        functools.update_wrapper(self, func)

    def __set_context(self, request: Request):
        self.context = request.query.get('context', '')

    def __set_id(self, request: Request):
        self.id = request.headers.get('X-Request-Id', '')

    def __construct_data(self, request: Request) -> OrderedDict:
        """
        Конструктор для поля data
        Вложенные поля должны быть в следующем порядке:
            * kind
            * fields
            * currentItemCount
            * itemsPerPage
            * startIndex
            * totalItems
            * любые другие поля
            * items
        :return OrderedDict:
        """
        pairs = []

        kind = self.data.pop('kind', None)
        if kind:
            pairs.append(("kind", kind))

        fields = self.data.pop('fields', None)
        if fields and isinstance(fields, (set, list, tuple)):
            pairs.append(("fields", ','.join(fields)))

        limit = self.data.pop('limit', None) or 0
        offset = self.data.pop('offset', None) or 0
        total = self.data.pop('total', None) or 0
        items = self.data.pop('items', None) or []

        if limit is not None:
            pairs.extend((("currentItemCount", len(items)),))

        if items:
            if limit and total:
                pairs.extend((
                    ("itemsPerPage", limit),
                    ("startIndex", offset),
                ))

                next_start_index = offset + limit
                if next_start_index < total:
                    next_link = request.url.update_query(startIndex=next_start_index)
                    next_link = settings.SELF_API_URL + next_link.path_qs
                    pairs.append(
                        ('nextLink',
                         str(next_link))
                    )

                previous_start_index = offset - limit
                if previous_start_index >= 0:
                    previous_link = request.url.update_query(startIndex=previous_start_index)
                    previous_link = settings.SELF_API_URL + previous_link.path_qs

                    pairs.append(
                        ('previousLink',
                         str(previous_link))
                    )

        pairs.append(("totalItems", total))

        if self.data:
            pairs.extend((k, v) for k, v in self.data.items())

        pairs.append(("items", items))

        return OrderedDict(pairs)

    def __construct_response(self, request: Request) -> OrderedDict:
        response = OrderedDict((
            ("apiVersion", self.api_version),
            ("context", self.context),
            ("id", self.id),
            ("method", self.method),
            ("data", self.__construct_data(request)),
        ))
        if not response["context"]:
            del response["context"]
        return response

    async def __call__(self, *args, **kwargs):
        request = self.this if isinstance(self.this, Request) else self.this.request

        self.__set_context(request)
        self.__set_id(request)
        self.data = await self.func(self.this, *args, **kwargs)
        self.data = change_dict_naming_convention(self.data)
        return return_json(self.__construct_response(request))


def api_call(func, method: str = None):
    """
    General decorator for Google JSON API Standard response

    :param func: request_handler function. Arity: fun/1 -> (serializable data)
    :param method: string with custom method name. If not set then current view + method name
    will be used as 'method'
    """
    def _decorator(self, *args, **kwargs):
        return ApiCall(func, this=self, method=method).__call__(*args, **kwargs)
    return _decorator


def serialize_exception(e: "Exception", location: str = None, r_id: str = None) -> OrderedDict:
    errors = []
    if isinstance(e, AggregatorHttpExceptions):
        for err in e.exceptions:
            err_info = DictWithoutNoneValue()
            err_info['domain'] = getattr(err, "domain", None)
            err_info["location"] = getattr(err, "location", None)
            err_info["locationType"] = getattr(err, "location_type", None)
            err_info["reason"] = getattr(err, "reason", err.__class__.__name__)
            err_info["message"] = getattr(err, "message", None)
            err_info["extendedHelp"] = getattr(err, "extended_help", None)
            err_info["sendReport"] = getattr(err, "send_report", None)
            errors.append(err_info)
    else:
        err_info = DictWithoutNoneValue()
        err_info["domain"] = getattr(e, "domain", settings.SERVICE_NAME)
        err_info["location"] = getattr(e, "location", location)
        err_info["locationType"] = getattr(e, "location_type", "module" if getattr(err_info, "location", "") else None)
        err_info["reason"] = getattr(e, "reason", e.__class__.__name__)
        err_info["message"] = getattr(e, "message", None)
        err_info["extendedHelp"] = getattr(e, "extended_help", None)
        err_info["sendReport"] = getattr(e, "send_report", None)
        errors.append(err_info)

    error_code = getattr(e, "status_code", 500)
    error_msg = exception_dict[error_code]

    error = OrderedDict((
        ("apiVersion", settings.API_VERSION),
        ("id", r_id),
        ("error", {
            "code": error_code,
            "message": error_msg,
            "errors": errors
        })
    ))

    if not error["id"]:
        del error["id"]
    return error
