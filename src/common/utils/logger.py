# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import logging
from datetime import datetime
from logging.config import dictConfig

import json_log_formatter
from raven_aiohttp import AioHttpTransport

import ujson
from conf import settings

getLogger = logging.getLogger


def json_record(self, message, extra, record):
    new_record = {
        'logtime': datetime.utcnow(),
        'loglevel': record.levelname,
        'logger': record.name,
        'filename': record.filename,
        'funcName': record.funcName,
        'lineno': record.lineno,
        'logmessage': message,
        'extra': {
            **extra
        },
    }

    if record.exc_info:
        new_record['exc_info'] = self.formatException(record.exc_info)

    return new_record


json_log_formatter.JSONFormatter.json_lib = ujson
json_log_formatter.JSONFormatter.json_record = json_record

LOG_DICT = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'default': {
            'class': 'json_log_formatter.JSONFormatter',
        },
        # [NGibaev 14.02.19] for dev purposes
        'simple': {
            'format': '\t'.join((
                '%(asctime)s',
                '%(levelname)s',
                '%(pathname)s:%(lineno)s',
                '%(message)s',))
        }
    },
    'handlers': {
        'console': {
            'level': settings.LOG_LEVEL,
            'class': 'logging.StreamHandler',
            # 'formatter': 'simple' | 'default'
            'formatter': settings.LOG_FORMATTER
        },
        'sentry': {
            'class': 'raven.handlers.logging.SentryHandler',
            'level': settings.SENTRY_LEVEL,
            'dsn': settings.SENTRY_DSN,
            'transport': AioHttpTransport,
            'auto_log_stacks': True
        },
    },
    'loggers': {
        'root': {
            'handlers': ('console', 'sentry'),
            'level': settings.LOG_LEVEL,
            'propagate': False,
        },
        'main': {
            'handlers': ('console', 'sentry'),
            'level': settings.LOG_LEVEL,
            'propagate': False,
        },
        'apps': {
            'handlers': ('console', 'sentry'),
            'level': settings.LOG_LEVEL,
            'propagate': False,
        },
        'dramatiq': {
            'handlers': ('console', 'sentry'),
            'level': settings.LOG_LEVEL,
            'propagate': False,
        },
        'sync.common.tasks': {
            'handlers': ('console', 'sentry'),
            'level': 'ERROR',
            'propagate': False,
        },
        'statsd': {
            'handlers': ('console', 'sentry'),
            'level': 'ERROR',
            'propagate': False,
        },
        'listen.sync_dramatiq': {
            'handlers': ('console', 'sentry'),
            'level': 'ERROR',
            'propagate': False,
        },
        'common': {
            'handlers': ('console', 'sentry'),
            'level': settings.LOG_LEVEL,
        },
        'api': {
            'handlers': ('console', 'sentry'),
            'level': settings.LOG_LEVEL,
        },
        'services': {
            'handlers': ('console', 'sentry'),
            'level': settings.LOG_LEVEL,
        },
        'conf': {
            'handlers': ('console', 'sentry'),
            'level': settings.LOG_LEVEL,
            'propagate': False,
        },
        'raven': {
            'level': 'ERROR',
            'handlers': ('console', 'sentry'),
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'ERROR',
            'handlers': ('console', 'sentry'),
            'propagate': False,
        },
    }
}

dictConfig(LOG_DICT)
app_logger = getLogger('main')
