# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import asyncio
import logging
import sys

from aiohttp.web_exceptions import HTTPBadRequest, HTTPNotFound

import common.utils.logger as _log_setup
from common.utils.exceptions import HTTPBadRequest as OurHTTPBadRequest
from common.utils.http_tools import return_json, serialize_exception

logger = logging.getLogger('main')


async def log_middleware(app, handler):
    async def middleware_handler(request):
        r_id = request.headers.get('X-Request-Id', '')
        logger.debug(f'{r_id} :: {request.rel_url}')
        try:
            response = await handler(request)
        except asyncio.CancelledError:
            pass
        except (HTTPNotFound, HTTPBadRequest, OurHTTPBadRequest) as e:
            error_info = serialize_exception(e, r_id=r_id)
            response_status = e.status_code
            return return_json(error_info, response_status)
        except Exception as e:
            logger.exception(f'{r_id} :: {e} in {handler.__module__}:{handler.__name__}')
            error_info = serialize_exception(e, handler.__module__.split(".")[-2:][0], r_id)
            response_status = error_info["error"]["code"]
            return return_json(error_info, response_status)
        except:
            logger.exception(f"Unexpected error in {r_id}: {sys.exc_info()[0]}")
        else:
            response_status = response.status
            return response
        finally:
            logger.info(f'{r_id} :: {request.rel_url} :: {response_status}')
    return middleware_handler
