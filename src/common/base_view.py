# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


from aiohttp import hdrs
from aiohttp.web import Response, View

import ujson
from common.utils.exceptions import HTTPNotFound
from common.utils.http_tools import api_call
from conf.settings import MIN_LIMIT, MIN_OFFSET

from .controller import Controller


def xor(a, b):
    return ((not a) and b) or (a and (not b))


class BaseAbstractView(View):

    async def _iter(self) -> dict:
        if self.request.method not in hdrs.METH_ALL:
            self._raise_allowed_methods()
        method = getattr(self, self.request.method.lower(), None)
        if method is None:
            self._raise_allowed_methods()
        pre_method = getattr(self, f'pre_{self.request.method.lower()}', None)
        after_method = getattr(self, f'after_{self.request.method.lower()}', None)

        if pre_method:
            await pre_method()

        resp = await method()

        if after_method:
            resp = after_method(resp)

        return resp

    async def _patch_valid_data(self, valid_data: dict) -> dict:
        patch_method = getattr(self, f'patch_data_{self.request.method.lower()}', None)
        if patch_method:
            return await patch_method(valid_data)
        return valid_data


class BaseView(BaseAbstractView):
    get_validator: type = None
    get_validator_header: type = None
    post_validator: type = None
    post_validator_body: type = None
    post_validator_header: type = None
    put_validator: type = None
    put_validator_body: type = None
    put_validator_header: type = None
    delete_validator: type = None
    delete_validator_body: type = None
    delete_validator_header: type = None
    pre_get: callable = None
    pre_post: callable = None
    pre_put: callable = None
    pre_delete: callable = None
    after_get: callable = None
    after_post: callable = None
    after_put: callable = None
    after_delete: callable = None
    patch_data_get: callable = None
    patch_data_post: callable = None
    patch_data_put: callable = None
    patch_data_delete: callable = None
    get_controller: Controller = None
    post_controller: Controller = None
    put_controller: Controller = None
    delete_controller: Controller = None
    model: type = None
    kind: str = None
    with_pagination: bool = False

    async def _validate_request(self) -> dict:
        validator = getattr(self, f'{self.request.method.lower()}_validator', None)
        validator_body = getattr(self, f'{self.request.method.lower()}_validator_body', None)
        validator_header = getattr(self, f'{self.request.method.lower()}_validator_header', None)
        query_params = {}
        validated_body = {}
        validated_headers = {}
        if validator:
            query_params = validator().validate(dict(self.request.query))
        if validator_body:
            validated_body = validator_body().validate_body(await self.request.json(loads=ujson.loads))
        if validator_header:
            validated_headers = validator_header().validate(dict(self.request.headers))
        query_params.update(validated_body)
        query_params.update(validated_headers)
        params = await self._patch_valid_data(query_params)
        if self.with_pagination:
            params['limit'] = params.get('limit', MIN_LIMIT)
            params['offset'] = params.get('offset', MIN_OFFSET)

        return params

    async def __get_from_model(self, params) -> dict:
        dao = self.model(self.request.app.pool)
        if not dao.has_collect:
            raise HTTPNotFound(message='Not found', reason='Not found get method in dao')
        # [NGibaev 04.09.18] pass request here in kwargs for compatibility with streams
        return await dao.collect(**params, request=self.request)

    async def __get_from_controller(self, params) -> dict:
        return await self.get_controller().call(params, self.request)

    @api_call
    async def get(self) -> dict:
        params = await self._validate_request()
        return {
            'kind': self.kind,
            'fields': params.get('fields'),
            'offset': params.get('offset'),
            'limit': params.get('limit'),
            **(
                await self.__get_from_controller(params)
                if self.get_controller else
                # [NGibaev 12.04.19] model call as fallback
                await self.__get_from_model(params)
            )
        }

    async def __post_from_model(self, params) -> dict:
        dao = self.model(self.request.app.pool)
        if not dao.has_save:
            raise HTTPNotFound(message='Not found', reason='Not found post method in dao')
        return await dao.save(params)

    async def __post_from_controller(self, params) -> dict:
        return await self.post_controller().call(params, self.request)

    @api_call
    async def post(self) -> dict:
        params = await self._validate_request()
        data = {
            'kind': self.kind,
            'fields': params.get('fields'),
            **(
                await self.__post_from_controller(params)
                if self.post_controller else
                {'items': await self.__post_from_model(params)}
            )
        }
        return data

    async def __put_from_model(self, params) -> dict:
        dao = self.model(self.request.app.pool)
        if not dao.has_update:
            raise HTTPNotFound(message='Not found', reason='Not found put method in dao')
        return await dao.update(params)

    async def __put_from_controller(self, params) -> dict:
        return await self.put_controller().call(params, self.request)

    @api_call
    async def put(self) -> dict:
        params = await self._validate_request()
        data = {
            'kind': self.kind,
            'fields': params.get('fields'),
            **(
                await self.__put_from_controller(params)
                if self.put_controller else
                {'items': await self.__put_from_model(params)}
            )
        }
        return data

    async def __delete_from_model(self, params) -> dict:
        dao = self.model(self.request.app.pool)
        if not dao.has_delete:
            raise HTTPNotFound(message='Not found', reason='Not found delete method in dao')
        # [NGibaev 12.04.19] unpack?
        # I think it should be unpacked, bcs ``delete`` method does not hold any data
        # except for filtering criteria so this method is similar to ``get``
        return await dao.delete(**params)

    async def __delete_from_controller(self, params) -> dict:
        return await self.delete_controller().call(params, self.request)

    @api_call
    async def delete(self) -> dict:
        params = await self._validate_request()
        data = {
            'kind': self.kind,
            'fields': params.get('fields'),
            **(
                await self.__delete_from_controller(params)
                if self.delete_controller else
                {'items': await self.__delete_from_model(params)}
            )
        }
        return data

    async def options(self, *args, **kwargs) -> Response:
        return Response(status=204)
