# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


from typing import List

from .service_base import Service, ServicesLinkedPipeline


class Controller:
    def prepare_input(self, params):
        """
        Implement by hand to properly format/group your data from request
        """
        raise NotImplementedError()

    def unpack_last_result(self, last_result):
        """
        Implement how to return last ServiceResult
        """
        raise NotImplementedError()

    def pipeline(self, params, request) -> List[Service]:
        """
        Implement by hand to build processing sequence
        """
        return []

    def __build_pipeline(self, params, request):
        return ServicesLinkedPipeline(*self.pipeline(params, request))

    async def call(self, params, request):
        seq = self.__build_pipeline(params, request)
        return self.unpack_last_result(await seq.emit(self.prepare_input(params)))
