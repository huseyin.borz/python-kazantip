# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import logging

import asyncpg

import ujson

from . import settings

logger = logging.getLogger(__name__)


def ujson_encoder(value):
    # тут пробрасываем значение как есть, т.к. мы сами его кодируем
    return value


def ujson_decoder(value):
    return ujson.loads(value)


async def init_connection(connection):
    await connection.set_type_codec(
        'jsonb', encoder=ujson_encoder, decoder=ujson_decoder,
        schema='pg_catalog'
    )


async def setup_database(app):
    logger.debug(f"Connecting to postgres to {settings.POSTGRES_DB_NAME} at {settings.POSTGRES_DB_HOST}")
    app.pool = await asyncpg.create_pool(dsn=settings.DATABASE_URL, init=init_connection,
                                         min_size=settings.DB_POOL_CONNECTIONS_MIN_SIZE,
                                         max_size=settings.DB_POOL_CONNECTIONS_MAX_SIZE)
