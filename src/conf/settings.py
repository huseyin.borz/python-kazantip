# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import logging
import sys
from os.path import isfile

from envparse import env
from schema import Or

if isfile('.env'):
    env.read_envfile('.env')


ENV = env('RUN_ENV', None)
if ENV not in ('test', 'dev', 'prod'):
    raise Exception(f"RUN_ENV variable must be one of [test, dev, prod], detected {ENV}")
DEBUG = ENV != 'prod'
IS_TEST_MODE = any('test' in arg for arg in sys.argv) or ENV == 'test'


################
# LOGGING CONF #
################
log_level_names = [
    logging.getLevelName(level)
    for level in (logging.CRITICAL,
                  logging.ERROR,
                  logging.WARNING,
                  logging.INFO,
                  logging.DEBUG,
                  logging.NOTSET)
]
error_log_level = logging.getLevelName(logging.ERROR)


def get_log_level_from_env(var_name, default):
    validator = Or(*log_level_names,
                   error=f'{var_name} failed. "{{}}" not in {log_level_names}')
    return validator.validate(env(var_name, default))


LOG_LEVEL = get_log_level_from_env('LOG_LEVEL', error_log_level)
LOG_FORMATTER = env('LOG_FORMATTER', default='default')

SENTRY_DSN = env('SENTRY_DSN', '')
SENTRY_LEVEL = get_log_level_from_env('SENTRY_LEVEL', error_log_level)


##################
# API HINTS CONF #
##################
API_VERSION = env('API_VERSION', "0.0.1")
SERVICE_NAME = env('SERVICE_NAME', 'example')

LISTEN_HOST = env('LISTEN_HOST', default='0.0.0.0')
LISTEN_PORT = env.int('LISTEN_PORT', default=8080)

SELF_API_URL = env('SELF_API_URL', 'SELF_URL_UNDEFINED')

ROUTE_PREFIX = env('ROUTE_PREFIX', '').rstrip('/')


###########
# DB CONF #
###########
POSTGRES_DB_NAME = env("POSTGRES_DB_NAME", "")
POSTGRES_DB_USER = env("POSTGRES_DB_USER", "")
POSTGRES_DB_PASSWORD = env("POSTGRES_DB_PASSWORD", "")
POSTGRES_DB_HOST = env("POSTGRES_DB_HOST", "")
POSTGRES_DB_PORT = env("POSTGRES_DB_PORT", 5432)
DB_DSN = f'postgres://{POSTGRES_DB_USER}:{POSTGRES_DB_PASSWORD}@{POSTGRES_DB_HOST}:{POSTGRES_DB_PORT}/{POSTGRES_DB_NAME}'
DATABASE_URL = env('DATABASE_URL', env('DSN', DB_DSN))
DB_POOL_CONNECTIONS_MIN_SIZE = env('DB_POOL_CONNECTIONS_MIN_SIZE', default=1, cast=int)
DB_POOL_CONNECTIONS_MAX_SIZE = env('DB_POOL_CONNECTIONS_MAX_SIZE', default=1, cast=int)


###################
# PAGINATION CONF #
###################
MIN_LIMIT = env('MIN_LIMIT', cast=int, default=20)
MIN_OFFSET = env('MIN_OFFSET', cast=int, default=0)


############
# ESB CONF #
############
ESB_URL = env('ESB_URL', 'http://0.0.0.0:8080/eventbus/v1/')
ESB_SENDER_NAME = env('ESB_SENDER_NAME', 'e2')


#################
# DRAMATIQ CONF #
#################
BROKER_URL = env('BROKER_URL', '')

PERIODIC_TASKS = (
    # ('crontab_format', 'module_path:called function'),
    # ('* * * * *', 'apps.example.tasks:print_current_date.send'),
)
