# Kazantip -- modular and declarative framework for web applications
#
# The MIT License (MIT)
#
# Copyright (c) 2018-2019 Nail Gibaev


import argparse
import os
import re

import conf.settings as s

MIGRATION_RE = re.compile(r'V(?P<num>\d{4})__[^.]+\.sql')


COMMANDS = ('isort', 'migrate', 'lint_apps',
            'create_migration', 'create_app',
            'run_server', 'run_worker')


def isort(_):
    os.system('isort -rc --atomic .')


def migrate(_):
    conn_str = ' '.join([f"host={s.POSTGRES_DB_HOST}",
                         f"port={s.POSTGRES_DB_PORT}",
                         f"dbname={s.POSTGRES_DB_NAME}",
                         f"user={s.POSTGRES_DB_USER}",
                         f"password={s.POSTGRES_DB_PASSWORD}"])
    os.system(f'pgmigrate -d conf/db_schema/ -c "{conn_str}" -t latest migrate')


def lint_apps(_):
    os.system(f'find apps -iname "*.py" | xargs pylint --rcfile=.pylintrc')


def create_migration(args):
    if not args.migration_name:
        print("Error: no migration name supplied")
        exit(1)
    migrations = sorted(f for f in os.listdir("conf/db_schema/migrations/") if f.endswith('.sql'))
    last_migration_num = int(MIGRATION_RE.search(migrations[-1])['num']) if migrations else 0
    migration_n = str(last_migration_num + 1).zfill(4)
    migration = f'V{migration_n}__{args.migration_name}.sql'
    res = os.system(f'touch conf/db_schema/migrations/{migration}')
    if res:
        print("Error: cant create migration")
        exit(res)
    print(f'CREATED: {migration} in conf/db_schema/migrations/')


def create_app(args):
    if not args.app_name:
        print("Error: no subapplication name supplied")
        exit(1)
    is_exist_app = [f for f in os.listdir("apps/") if f == args.app_name]
    if is_exist_app:
        print(f"Error: application with given name \"{args.app_name}\" already exists")
        exit(1)
    res = os.system(f"mkdir apps/{args.app_name}")
    if res:
        print("Error: cant create application catalog")
        exit(res)
    path = f"apps/{args.app_name}"
    os.system(f'echo "# routes.py -- module for mapping http routes to views" > {path}/routes.py')
    os.system(f'echo "# views.py -- module for http request handlers" > {path}/views.py')
    os.system(f'echo "# daos.py -- module for declaring DAO classes" > {path}/daos.py')
    os.system(f'echo "# specs.py -- module for declaring specifications" > {path}/specs.py')
    os.system(f'echo "# controllers.py -- module for controllers for linking up services workflow" > {path}/controllers.py')  # noqa
    os.system(f'echo "# services.py -- module for services that serve as units of business logic" > {path}/services.py')
    os.system(f'echo "# dtos.py -- module for DTO declarations for data passing between services" > {path}/dtos.py')
    os.system(f'touch {path}/__init__.py')
    print(f"Created subapplication {args.app_name}")


def run_server(_):
    os.system('python main.py')


def run_worker(_):
    os.system('./start_dramatiq_workers.sh')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Management CLI for KAZANTIP application")
    parser.add_argument('command', metavar='CMD', type=str, nargs='?',
                        help='Command', choices=COMMANDS)
    parser.add_argument('-m', dest='migration_name', action='store',
                        help='Migration name (for create_migration command)', default='')
    parser.add_argument('-n', dest='app_name', action='store',
                        help='Name of subapplication in apps/ (for create_app command)', default='')

    args = parser.parse_args()
    if not args.command:
        print("Error: no command supplied")
        exit(1)
    globals()[args.command](args)
